var fs = require('fs'),
    path = require('path'),
    util = require('util'),
    Deferred = require('JQDeferred'), $ = { Deferred: Deferred, when: Deferred.when };

module.exports = function(opts) {
    var BitcasaItem = require('./BitcasaItem')(opts),
        $request = require('./request')(opts).$request,

        BitcasaRootDirectory = new BitcasaItem({
            name: '/', 
            path: '/', 
            type: 1
        }),

        BitcasaCore = {
            checkSessionId: function() {
                return BitcasaRootDirectory.list().then(function() {
                    opts.debug && console.log('SessionID is valid!');
                    return true;
                }, function() {
                    opts.debug && console.log('SessionID is invalid!');
                    return false;
                });
            },

            BitcasaRootDirectory: BitcasaRootDirectory,            

            ls: function(path) {
                return this.itemAtPath(path).then(function(result) {
                    if(result.isDirectory) {
                        return result.list();
                    } else {
                        return [result];
                    }
                });
            },

            itemAtPath: function(path) {
                opts.debug && console.log('[Bitcasa] itemAtPath("%s")', path);
                var dir = path || '/',
                    dirs = dir.split('/'),
                    last;

                if(!dirs[0]) {
                    dirs.shift(); //remove leading /
                }
                while(dirs.length && !last) {
                    last = dirs.pop(); //remove last element from path
                }

                if(!last) return $.Deferred().resolve(BitcasaRootDirectory);
                return BitcasaRootDirectory.list(dirs.join('/')).then(function(items) {
                    var found;

                    items.some(function(item) {
                        if(item.name === last) {
                            found = item;
                            return true;
                        }
                    });

                    return found || $.Deferred().reject('404 - Could not find "' + last + '".');;
                });
            },

            upload: function(path, filesOrPath) {
                opts.debug && console.log('[Bitcasa] uploading "%s" to "%s"', filesOrPath, path);
                
                if(util.isArray(filesOrPath) || fs.statSync(filesOrPath).isFile()) {
                    //single file or list of files specified, so we let upload handle this
                    return BitcasaCore.itemAtPath(path).then(function(item) {
                        return item.upload(filesOrPath);
                    });
                }

                // if(fileStats.isDirectory()) {
                //     //user specified a directory, so we create this directory (if it does not exists)
                //     // and then upload every file from the client to bitcasa
                //     var dirName = path.basename(filePath),
                //         uploadAll = function(bitcasaItem) {

                //         };
                //     return BitcasaCore.itemAtPath(path).then(
                //         //the specified path already exists, great, we just go on
                //         uploadAll,

                //         //the specified path does not exist just yet, we create it and then go on
                //         function(err) {
                //             var _p = path.split('/'),
                //                 dirName = _p.pop(),
                //                 parent = _p.join('/');

                //             return BitcasaCore.itemAtPath(parent).then(function(parentBitcasaItem) {
                //                 return parentBitcasaItem.createSubdirectory(dirName).then(uploadAll);
                //             });
                //         }
                //     );
                // }
            }
    };

    return BitcasaCore;
}