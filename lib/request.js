var request = require('request'),
    Deferred = require('JQDeferred'), $ = { Deferred: Deferred, when: Deferred.when };

module.exports = function(opts) {
    var getSessionId = function() {
            if(typeof opts.sessionId === 'function') {
                var user;
                opts.sessionId(function(u) {
                    user = u;
                });
                console.log('Asked.. Got ' + user);
                return user;
            }
            return opts.sessionId;
        }
        reqHeaders = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1738.0 Safari/537.36',
            'X_REQUESTED_WITH': 'XMLHttpRequest',
            'Cookie': 'sessionid='+getSessionId()
        },
        $_request = function(url, options, requestCallback) {
            var $def = $.Deferred(),
                options = options || {};

            options.url = url;
            options.headers = options.headers || {};
            options.headers['Cookie'] = options.headers['Cookie'] || reqHeaders['Cookie'];
            options.strictSSL = false;

            request(options, (requestCallback&&requestCallback.bind($def)) || function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        var resp = body;
                        if(!options.plainResponse) {
                            resp = JSON.parse(body);
                        }
                        
                        if(resp.error) return $def.reject(resp.error);
                        return $def.resolve(resp);
                    } else {
                        console.warn('[request] Error happened: ', arguments);
                        return $def.reject(body);
                    }
                }
            );

            return $def;
        };

    return {
        $MYrequest: function(url, options, requestCallback) {
            url = opts.urls.downloader + url;

            options = options || {};
            options.headers = options.headers || {};
            options.headers['User-Agent'] = reqHeaders['User-Agent'];
            options.headers['X_REQUESTED_WITH'] = reqHeaders['X_REQUESTED_WITH'];
            
            return $_request.apply(this, arguments);
        },
        $request: function(url, options, requestCallback) {
            url = opts.urls.api + url;
            url += (url.indexOf('?') !== -1?'&':'?') + 'access_token=' + getSessionId();
            return $_request.apply(this, arguments);
        }
    }
}