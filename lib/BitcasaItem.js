var fs = require('fs'),
    Path = require('path'),
    Deferred = require('JQDeferred'), $ = { Deferred: Deferred, when: Deferred.when };

function updateFromResponse(bitcasaItem, response) {
    ['name', 'mtime', 'ctime', 'birth_time', 'type'].forEach(function(field) {
        bitcasaItem[field] = response[field];
    });
    bitcasaItem._bitcasaPath = response.path;
    bitcasaItem.path = Path.join(((bitcasaItem.parent&&bitcasaItem.parent.path)||''), response.name);
    bitcasaItem.isDirectory = response.type === 1;
    bitcasaItem.isFile = response.type === 0;
}

module.exports = function(opts) {
    var _r = require('./request')(opts),
        $MYrequest = _r.$MYrequest,
        $request = _r.$request,

        BitcasaItem = function(data, parent) {
            var itm = {
                // _data: data,
                parent: parent,

                items: undefined,
                list: function(subdirs) {
                    if (this.isFile) {
                        return $.Deferred().reject('Cannot "list()" a file!');
                    }

                    var nextSubdir, nextSubdirs;
                    if (subdirs) {
                        nextSubdirs = subdirs.split('/');
                        nextSubdir = nextSubdirs.shift();
                    }
                    // console.log('subdirs: ', subdirs, 'nextSubdir: ', nextSubdir, 'nextSubdirs: ', nextSubdirs)


                    var path = this._bitcasaPath && this._bitcasaPath.substr(1),
                        $def;
                    if (this.items) {
                        $def = $.Deferred().resolve(this.items);
                    } else {
                        var self = this;
                        $def = $request('/folders/' + path).then(function(data) {
                            return data.result.items.map(function(item) {
                                return new BitcasaItem(item, self);
                            });
                        });
                    }

                    return $def.then(function(items) {
                        var searchedItem;
                        
                        if (nextSubdir) {
                            items.some(function(bitcasaItem) {
                                if (nextSubdir && nextSubdir === bitcasaItem.name) {
                                    searchedItem = bitcasaItem;
                                    return true;
                                }
                            });
                        }

                        this.items = items;

                        if (nextSubdir && searchedItem) {
                            // console.log('nextSubdir: ', nextSubdir, ' searchedItem: ', searchedItem);
                            return searchedItem.list(nextSubdirs.join('/'));
                        } else if (nextSubdir && !searchedItem) {
                            return $.Deferred().reject('404 - Could not find "' + nextSubdir + '" in "' + this.name + '".');
                        } else {
                            return items;
                        }
                    }.bind(this));
                },

                //note: this will include your sessionId, use with care!
                _getDownloadLink: function() {
                    if(this.isFile) {
                        return opts.urls.downloadPrefix + '/files/' + this.id + '/' + this.name + '?access_token=' + opts.sessionId;
                    }
                },

                getFileContent: function() {
                    if(this.isFile) {
                        return $request('/files/' + this.id + '/' + this.name, { plainResponse: true });
                    } else {
                        return $.Deferred().reject('Cannot get content of directory!');
                    }
                },

                _refresh: function(self) {
                    if(this.items) {
                        this.items = undefined;
                        return this.list().then(function() {
                            //return the original dir instead of the list of items, for chaining
                            return this;
                        }.bind(this));
                    } else {
                        return $.Deferred().resolve(this);
                    }
                },

                delete: function() {
                    var type;
                    if(this.isFile) {
                        type = 'files';
                    } else if(this.isDirectory) {
                        type = 'folders';
                    }

                    if(type) {
                        return $request('/' + type, {
                            method: 'DELETE',
                            form: {
                                path: this._bitcasaPath
                            }
                        }).then(function() {
                            console.log('Item deleted successfully: ', this);
                        });
                    } else {
                        return $.Deferred().reject('Unknown Type!');
                    }
                },

                createSubdirectory: function(name) {
                    if(this.isDirectory) {
                        return $request('/folders/' + this._bitcasaPath, {
                            method: 'POST',
                            form: {
                                folder_name: name
                            }
                        }).then(function() {
                            this.items = undefined;
                            return this.list().then(function(items) {
                                var newDir;
                                items.some(function(item) {
                                    if(item.name === name) {
                                        newDir = item;
                                        return true;
                                    }
                                });

                                return newDir || $.Deferred().reject('Could not find newly created Subdiretory!');
                            });
                        }.bind(this));
                    } else {
                        return $.Deferred().reject('Cannot create Subdiretory on a file!');
                    }
                },

                rename: function(destinationName) {
                    var self = this,
                        type;
                    if(this.isFile) {
                        type = 'files';
                    } else if(this.isDirectory) {
                        type = 'folders';
                    }

                    if(type) {
                        var self = this;
                        return $request('/' + type + '?operation=rename', {
                            method: 'POST',
                            form: {
                                from: this._bitcasaPath,
                                filename: destinationName,
                                exists: 'fail'
                            }
                        }).then(function(resp) {
                            console.log('Item renamed successfully: ', self);
                            updateFromResponse(self, resp.result.items[0]);
                            
                            //refresh if this directory was listed before, as items contain old path
                            return self._refresh();
                        });
                    } else {
                        return $.Deferred().reject('Unknown Type!');
                    }
                },

                move: function(destinationDir, destinationName) {
                    var type, destinationName = destinationName||this.name;
                    if(this.isFile) {
                        type = 'files';
                    } else if(this.isDirectory) {
                        type = 'folders';
                    }

                    if(type) {
                        var self = this;
                        return $request('/' + type + '?operation=move', {
                            method: 'POST',
                            form: {
                                from: this._bitcasaPath,
                                filename: destinationName,
                                to: destinationDir._bitcasaPath,
                                exists: 'fail'
                            }
                        }).then(function(resp) {
                            console.log('Item moved successfully: ', self);
                            updateFromResponse(self, resp.result.items[0]);
                            
                            //refresh if this directory was listed before, as items contain old path
                            return self._refresh();
                        });
                    } else {
                        return $.Deferred().reject('Unknown Type!');
                    }
                },

                _upload: function(fileArray) {
                    var self = this,
                        multipart = [];

                    fileArray.forEach(function(filePath) {
                        multipart.push({
                            'Content-Disposition': 'form-data; name="file"; filename="' + Path.basename(filePath) + '"',
                            body: fs.readFileSync(filePath)
                        });
                    });

                    return $request('/files' + this._bitcasaPath + '?exists=rename', {
                        method: 'PUT',
                        headers: {
                            'content-type': 'multipart/form-data'
                        },
                        multipart: multipart
                    }).then(
                        function(resp) {
                            //this was a single upload, so we return a single BitcasaItem
                            // after we refreshed ourselves
                            return self._refresh().then(function() {
                                return resp.result.items.map(function(bitcasaFile) {
                                    return new BitcasaItem(bitcasaFile, self);
                                });
                            });
                        }
                    )
                },

                upload: function(filesOrPath) {
                    // var self = this,
                    //     $def = $.Deferred();

                    // fs.readFile(filePath, function (err, data) {
                    //     if (err) {
                    //         return $def.reject(err);
                    //     }

                    //     self._upload([{
                    //         filePath: filePath,
                    //         data: data
                    //     }]).then(
                    //         $def.resolve,
                    //         $def.reject,
                    //         $def.notify
                    //     );
                    // }.bind(this));

                    // return $def;
                    var files;
                    if(typeof filesOrPath === 'string') {
                        return this._upload([filesOrPath]).then(function(items) {
                            return items[0];
                        });
                    } else {
                        return this._upload(filesOrPath);
                    }
                }
            };

            updateFromResponse(itm, data);
            // copy some more fields
            ['id', 'incomplete', 'size', 'mime'].forEach(function(field) {
                itm[field] = data[field];
            });

            return itm;
        }
        
    return BitcasaItem;
}