var Deferred = require('JQDeferred'), $ = { Deferred: Deferred, when: Deferred.when },

    sessionDls = {}, 
    waiting = [], 
    waitingTimeout;

module.exports = function(opts) {
    var $MYrequest = require('./request')(opts).$MYrequest,
        my = {
            _lastStatus: undefined,
            _getStatus: function() {
                return $MYrequest('/uploader/get-upload-status').then(function(dataArray) {
                    return this._lastStatus = dataArray[0];
                }.bind(this));
            },

            getDownloads: function() {
                return this.getStatus().then(function(status) {
                    return {
                        sessionDls: sessionDls,
                        bitcasaDls: status.uploads
                    };
                });
            },

            getStatus: function() {
                if(waitingTimeout) {
                    //if waiting timeout is set, we must not request the status from BC, as this would mess up our download states
                    // (BC only returns every status update once)
                    return $.Deferred.resolve(this._lastStatus);
                } else {
                    return this._getStatus();
                }
            },

            cancelDownload: function(dl_id) {
                opts.debug && console.log('Cancelling download %s', dl_id)
                return $MYrequest('/uploader/cancel?upload_id=' + dl_id).then(function() {
                    console.log('[BitcasaDownloader] Download %s cancelled', dl_id)
                });
            },

            cleanupDownloadQueue: function() {
                return my.getStatus().then(function(data) {
                    var $defs = [];
                    data.uploads.forEach(function(ul) {
                        $defs.push(my.cancelDownload(ul.id));
                    });

                    return $.when.apply($, $defs);
                });
            },

            download: function(url) {
                opts.debug && console.log('[BitcasaDownloader] Download started: ', url);
                return $MYrequest('/uploader/download-to-bitcasa', {
                    method: 'POST',
                    form: {
                        file: url,
                        cookies: []
                    }
                }, function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        var data = JSON.parse(body);
                        opts.debug && console.log('[BitcasaDownloader] Download submitted to bitcasa: ', url);
                        sessionDls[data.upload_id] = {
                            bitcasaInfo: data,
                            $def: this
                        }

                        waiting.push(data.upload_id);
                        wait();
                    } else {
                        console.warn(arguments)
                        this.reject();
                    }
                });
            },
        };

    function wait() {
        if(waiting.length > 0 && !waitingTimeout) {
            waitingTimeout = setTimeout(_wait, 500);
        }
    }

    function _wait() {
        my._getStatus().done(function(data) {
            // console.log(data);

            waiting = waiting.filter(function(id) {
                //search in currently active dls and remove if not in the array
                var stillDownloading = true, error;
                data.uploads.some(function(ul) {
                    if(ul.id === id) {
                        if(ul.status === 'error') {
                            stillDownloading = false;
                            error = ul.message;
                            return true;
                        }
                        if(!!ul.done) {
                            sessionDls[id].$def.notify(ul);
                        }
                        sessionDls[id].bitcasaInfo.downloadStatus = ul;
                        stillDownloading = ul.done < 100;
                        return true;
                    }
                });


                if(error) {
                    console.warn('[BitcasaDownloader] Error (%s): %s', id, error);
                    sessionDls[id] && sessionDls[id].$def.reject(error);
                    return false;
                }

                if(!stillDownloading) {
                    opts.debug && console.log('[BitcasaDownloader] Download finished %s', id);
                    sessionDls[id] && sessionDls[id].$def.resolve(sessionDls[id].bitcasaInfo);
                }

                return stillDownloading;
            });

            if(waiting.length > 0) {
                waitingTimeout = setTimeout(_wait, 100);
            }
        });
    }
    return my;
}