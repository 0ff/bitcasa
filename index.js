
module.exports = function(sessionId) {
    var debug = process.env.BITCASA_DEBUG,
        opts = {
            sessionId: sessionId,
            debug: debug,
            urls: debug?{
                api: 'https://localhost:50139/v1',
                downloader: 'https://localhost:50138',
                downloadPrefix: 'https://files.api.bitcasa.com/v1'
                // downloadPrefix: 'https://localhost:50139/v1'
            }:{
                api: 'https://developer.api.bitcasa.com/v1',
                downloader: 'https://my.bitcasa.com',
                downloadPrefix: 'https://files.api.bitcasa.com/v1'
            }
        },
        $request = require('./lib/request')(opts).$request,
        BitcasaItem = require('./lib/BitcasaItem')(opts),
        BitcasaCore = require('./lib/BitcasaCore')(opts),
        BitcasaDownloader = require('./lib/BitcasaDownloader')(opts),

        bitcasa = {
            _opts: opts,
            _downloader: BitcasaDownloader,
            downloader: {
                download: function(url, destinationDir, destinationName) {
                    var $def = BitcasaDownloader.download.apply(BitcasaDownloader, arguments);

                    if(destinationDir) {
                        $def.then(function(download) {
                            opts.debug && console.log('Download finished', download);
                            return bitcasa.itemAtPath('/Downloads/' + download.downloadStatus.name).then(function(downloadedFile) {
                                opts.debug && console.log('got downloadedFile', downloadedFile);
                                return bitcasa.itemAtPath(destinationDir).then(function(destDir) {
                                    opts.debug && console.log('got destDir', destDir);
                                    return downloadedFile.move(destDir, destinationName||downloadedFile.name).then(function(itm) {
                                        return itm;
                                    });
                                });
                            });
                        });
                    }

                    return $def;
                },

                getStatus: BitcasaDownloader.getStatus.bind(BitcasaDownloader),
                getDownloads: BitcasaDownloader.getDownloads.bind(BitcasaDownloader),
                cancelDownload: BitcasaDownloader.cancelDownload.bind(BitcasaDownloader),
                cleanupDownloadQueue: BitcasaDownloader.cleanupDownloadQueue.bind(BitcasaDownloader)
            },

            _core: BitcasaCore,
            checkSessionId: BitcasaCore.checkSessionId.bind(BitcasaCore),
            ls: BitcasaCore.ls.bind(BitcasaCore),
            itemAtPath: BitcasaCore.itemAtPath.bind(BitcasaCore),
            upload: BitcasaCore.upload.bind(BitcasaCore)
            
        };

    return bitcasa;
}