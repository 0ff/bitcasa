# bitcasa
A nodejs Wrapper for the bitcasa API

**WARNING: This is extremely experimental and might possibly delete all your files, do harm to your children and what not.**

## Working
### file-downloads (using the unofficial chrome-api)
* Start file-download
* Monitor file-download 
    * be notified with progress
    * be notified when download finishes
    * be notified of errors
* clear the download queue
* cancel select downloads

### API-functions
* list directory
* get item by path
* delete item (directory/file)
* create subdirectory (directory only)

## Usage
```javascript
    var Bitcasa = require('bitcasa'),
        bitcasa = new Bitcasa('YOUR API TOKEN');

    bitcasa.downloader.download('http://path/to/your/file').then(
        function(download) {
            //file was downloaded successfully
        },

        function(error) {
            //file could not be downloaded because of an error
        },

        function(downloadStatus) {
            //progress-notification
            //0.0 <= downloadStatus.done <= 100.0
        }
    )

    //cancel a download
    bitcasa.downloader.cancelDownload("strange-download-id");
```

## TODO
* support the user in getting an access-token (sessionid)
    * not before bitcasa supports real OAUTH
    * token lives for plenty of time, no need to get a new token too soon
* support file-actions
    * rename
    * copy
    * delete
* support directory-actions
    * rename
    * copy